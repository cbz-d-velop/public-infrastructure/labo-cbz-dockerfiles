# Docker-Image-As-Service

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![CI Status](https://img.shields.io/badge/CI-success-brightgreen)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Group](https://img.shields.io/badge/Group-CBZ%20D--velop-blue)
![Group](https://img.shields.io/badge/Group-Adeptus%20Automata%20Silici-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Cbz-blue)

## Description

![Tag: Jenkins](https://img.shields.io/badge/Tech-Jenkins-orange)
![Tag: GitLab-CI](https://img.shields.io/badge/Tech-GitLab--Ci-orange)
![Tag: Docker](https://img.shields.io/badge/Tech-Docker-orange)
![Tag: DevOps](https://img.shields.io/badge/Tech-DevOps-orange)
![Tag: DevSecOps](https://img.shields.io/badge/Tech-DevSecOps-orange)
![Tag: GitOps](https://img.shields.io/badge/Tech-GitOps-orange)
![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: RedHat](https://img.shields.io/badge/Tech-RedHat-orange)

This Git repository serves as a valuable resource for managing Dockerfiles and associated scripts. Docker containers have become a cornerstone of modern software deployment, and this repository is designed to simplify the organization and maintenance of Docker images.

In addition to Dockerfiles, this repository also contains scripts that facilitate various tasks, such as managing image tags and versions, loading variables, and importing files into Docker images. These scripts streamline the Docker image creation and management process, making it easier to maintain a consistent and efficient containerized application stack.

Whether you're developing microservices, maintaining containerized applications, or building custom Docker images, this repository simplifies the process of Docker image management. It provides a structured and organized approach to Dockerfile storage and associated scripts, promoting efficiency and consistency in your containerization workflow.

By utilizing this Git repository, you can take full advantage of Docker's flexibility and portability while maintaining a well-structured and organized Docker image management strategy.

## Usage

The following commands will help you set up and initialize the necessary tools and configurations for the project:

1. Install Husky, initialize it, install `validate-branch-name`, and run `commitizen` to standardize commit messages.
2. Initialize the local secrets database using `detect-secrets` to scan for any secrets in the codebase, excluding the `node_modules` directory.

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint Dockerfiles
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest hadolint --ignore DL3018 --ignore DL3001 --ignore DL3013 --ignore DL3008 --ignore DL3009 --ignore DL3015 ./*/*/Dockerfile
```

## Build a new image

You can build an image from a Dockerfile using the `./build` script. This script allows you to build your image while loading variables from the .env file (don't forget to edit the script if you add any variables).

This bash script automates the building and tagging of Docker images based on specified criteria and arguments. It also handles versioning and tagging of the images. To use this script, follow these steps:

1. Create a `.env` file with environment variables such as `MAINTAINER`, `MAINTAINER_ADDRESS`, `DEFAULT_USER`, `DEFAULT_GROUP`, and `DEFAULT_WORKDIR`.
2. Optionally, create a `.tag` file to maintain versioning information.
3. Execute the script with appropriate arguments.

The script supports the following arguments:

* `--major`: Perform a major build, incrementing the major version and resetting the minor version.
* `--test`: Perform a test build with a `-test` suffix.
* `--cicd`: Perform a CI/CD build with a date suffix.
* `--push`: Perform a minor build, incrementing the minor version.
* No argument: Perform a test build by default.

## Test an image

You can test the images using the `structure-test` script. This script allows you to run tests defined in a configuration file `tests-config.yml` to ensure the integrity and functionality of your Docker images.

To use the `structure-test` script, follow these steps:

* Create a `tests-config.yml` file with the necessary test configurations.
* Execute the `structure-test` script to run the tests against your Docker images.
* The `tests-config.yml` file should define the tests you want to run, such as checking for the presence of specific files, verifying environment variables, and ensuring that certain commands execute successfully.

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-10-03: First Init

* First init of this project by Lord Robin Crombez

### 2023-10-05: Added script for build

* Build script available to load vars, build, push, tag
* Argument --major available for Major versionning
* Added some Dockerfile

### 2023-10-06: Test / Lint pipeline

* This repository have a CI/CD to lint Dockerfiles (setted to lates)
* This repository have a CI/CD to build Dockerfiles in --test-build mode (setted to lates)
* This repository can be forked and modified to lint, build, analyse and push image from Dockerfile

### 2023-11-02: Docker Image As Service

* All images are now builded and updated by the CICD pipeline
* New tag for the build with the current version / latest

### 2024-02-19: Updates majors

* New CICD for image building
* Added Docker Dind for Ansible dev
* Added Debian 12 for Ansible dev
* Added Ubuntu 22 for Ansible dev
* Fix all images build
* Added for major versions
* Added passlib python lib for password hash
* Added Alpine Git, to publish

### 2024-03-26/25: New CICD

* Images follow a new CICD besed on DevSecOps and AS SERVICE
* Each image / Dockerfile is analysed separatly
* Added Sonarqube, for global quality
* Added Hadolint, specialized support
* Added detect-secrets for secret detection in images
* Added Docker Scout as layer analysis

### 2024-03-29: Jenkins CI

* Added a Jenkins CI to dayli build images based on cron
* Reworks on build script, added/updated options

### 2024-05-19: GitLab / Jenkins CI

* Rework on CI names
* Will not edit containers because not mandatory
* Added artefact analysis upload

### 2024-12-21: Global refactoring for Mk.4

* Rework all images
* Simplifications made for devops tools
* Use packages when necessary
* Need to create READMEs
* Need work on the NGINX image

### 2024-12-22: Added READMEs and automations

* Added all READMEs
* Added Git hooks and security automation
* Reset all tags to 0.1
* Rework on build script
* GitLab CICD need to be reworked, waiting for runners
* Added Ubutnu 24 image

### 2024-12-23: Adde CICD

* Edited CICD and automations
* Added a Jenkinsfile to execute daylis build based on image type and names
* Added SonarQube certificates

### 2024-12-25: Test of images

* Added a nex image to run tests inside others
* Added all tests for all images
* Removers checks insides Dockerfile, since tests are now available
* Edited Jenkins/GitLab CICD to launch tests

### 2025-01-29: NodeJS and NGINX

* Add NodeJS 20 image, based on Alpine
* Add NGINX image, based on RedHat in rootless mode

### 2025-01-31: Major tagging

* Set and publish all images with major tag

## Authors

* Lord Robin Crombez

## Sources

* [husiang/yamllint](https://hub.docker.com/r/chusiang/yamllint/dockerfile)
* [Installing Python in Alpine Linux](https://www.askpython.com/python/examples/python-alpine-linux)
* [Install Python on Alpine Linux](https://devcoops.com/install-python-on-alpine-linux/)
* [alpine:3.18](https://hub.docker.com/layers/library/alpine/3.18/images/sha256-48d9183eb12a05c99bcc0bf44a003607b8e941e1d4f41f9ad12bdcc4b5672f86?context=explore)
* [j2lint](https://github.com/aristanetworks/j2lint)

# Labo-CBZ Docker image: k8s/redhat-nginx-ssl

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![CI Status](https://img.shields.io/badge/CI-success-brightgreen)
![Language Status](https://img.shields.io/badge/language-Dockerfile-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Cbz-blue)

## Description

![Tag: Docker](https://img.shields.io/badge/Tech-Docker-orange)

This Docker image is designed to be a builder of `JavaScript/TypeScript` code using `NPM/NodeJS`. It includes all necessary dependencies and configurations to facilitate testing and development with `NPM/NodeJS` in version `20`. Designed to be used inside a `Kubernetes` cluster, this image use `RedHat UBI` as base image for security and corporate uses cases.

You can import your custom `NGINX` configuration file as a base 64 file passed with `ENV` variables. Same thing for your certificates.

## Environment variables

- `VERSION` - The version of the image.
- `MAINTAINER` - The maintainer of the image.
- `MAINTAINER_ADDRESS` - The email address of the maintainer.
- `DEFAULT_USER` - The default user to be created in the image.
- `DEFAULT_USER_ID` - The user ID for the default user.
- `DEFAULT_GROUP` - The default group to be created in the image.
- `DEFAULT_GROUP_GID` - The group ID for the default group.
- `DEFAULT_USER_HOME` - The home directory for the default user.
- `DEFAULT_WORKDIR` - The default working directory.
- `NODEJS_VERSION` - The version of Node.js to be installed.

## Build a new image

You can build an image from a Dockerfile using the `./build` script. This script allows you to build your image while loading variables from the .env file (don't forget to edit the script if you add any variables).

This bash script automates the building and tagging of Docker images based on specified criteria and arguments. It also handles versioning and tagging of the images. To use this script, follow these steps:

1. Create a `.env` file with environment variables such as `MAINTAINER`, `MAINTAINER_ADDRESS`, `DEFAULT_USER`, `DEFAULT_GROUP`, and `DEFAULT_WORKDIR`.
2. Optionally, create a `.tag` file to maintain versioning information.
3. Execute the script with appropriate arguments.

The script supports the following arguments:

- `--major`: Perform a major build, incrementing the major version and resetting the minor version.
- `--test`: Perform a test build with a `-test` suffix.
- `--cicd`: Perform a CI/CD build with a date suffix.
- `--push`: Perform a minor build, incrementing the minor version.
- No argument: Perform a test build by default.

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

## Authors

- Lord Robin Crombez

## Sources

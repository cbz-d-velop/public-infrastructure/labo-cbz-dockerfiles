#!/bin/sh

echo "${NGINX_CONF_BASE64}" | base64 -d > /etc/nginx/nginx.conf
echo "${NGINX_SSL_CERT_BASE64}" | base64 -d > /etc/nginx/ssl/server-cert.pem.crt
echo "${NGINX_SSL_KEY_BASE64}" | base64 -d > /etc/nginx/ssl/server-key.pem.key

nginx -g 'daemon off;'
ARG NODEJS_VERSION="20"
FROM node:${NODEJS_VERSION}-alpine AS nodejs_base

# FROM dind
FROM docker@sha256:826fc608d01af1f6fba86ee2df8332d10d7c0a3361d95a02dbd20d2c122496c8

ARG VERSION=""
ARG MAINTAINER=""
ARG MAINTAINER_ADDRESS=""
ARG DEFAULT_USER=""
ARG DEFAULT_USER_ID=""
ARG DEFAULT_GROUP=""
ARG DEFAULT_GROUP_GID=""
ARG DEFAULT_USER_HOME=""
ARG DEFAULT_WORKDIR=""
ARG DOCKER_API_TIMEOUT=""
ARG DOCKER_SCOUT_VERION=""

LABEL "author"=${MAINTAINER} \
      "contact"=${MAINTAINER_ADDRESS}

ENV DOCKER_API_TIMEOUT=${DOCKER_API_TIMEOUT} \
    DOCKER_CLIENT_TIMEOUT=${DOCKER_API_TIMEOUT} \
    COMPOSE_HTTP_TIMEOUT=${DOCKER_API_TIMEOUT}

# Check if the specified group exists, and create it if it does not exist, with the specified GID
# Check if the specified user exists, and create it if it does not exist, with the specified UID, group, home directory, and shell
# Create the specified work directory, set its permissions to 0700, and change its ownership to the specified user and group
RUN if ! getent group "${DEFAULT_GROUP}" > /dev/null 2>&1; then \
    addgroup --gid "${DEFAULT_GROUP_GID}" "${DEFAULT_GROUP}"; \
    fi && \
    if ! id -u "${DEFAULT_USER}" > /dev/null 2>&1; then \
    adduser --uid "${DEFAULT_USER_ID}" --ingroup "${DEFAULT_GROUP}" \
    --home "${DEFAULT_USER_HOME}" --shell "/bin/bash" --disabled-password "${DEFAULT_USER}"; \
    fi && \
    mkdir -p "${DEFAULT_WORKDIR}" && \
    chmod 0700 -R "${DEFAULT_WORKDIR}" && \
    chown -R "${DEFAULT_USER}:${DEFAULT_GROUP}" "${DEFAULT_WORKDIR}" && \
    apk add --no-cache bash

# Update the host, upgrade existing packages, and install necessary packages
# Install basic requirements and additional packages listed in packages.txt
# Install Python packages required for Ansible run/tests listed in requirements.txt
WORKDIR "${DEFAULT_USER_HOME}"
COPY --chown="root:root" ./packages.txt /tmp/packages.txt
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apk update && xargs -a /tmp/packages.txt apk add --no-cache
ENV HOME=$DEFAULT_USER_HOME
ENV PATH="/${HOME}/.local/bin:${PATH}"

# Add default user to docker group
RUN addgroup "${DEFAULT_USER}" docker

# Install Docker Scout CLI
USER ${DEFAULT_USER}:${DEFAULT_GROUP}
ADD https://github.com/docker/scout-cli/releases/download/v${DOCKER_SCOUT_VERION}/docker-scout_${DOCKER_SCOUT_VERION}_linux_amd64.tar.gz /tmp/docker-scout.tar.gz
RUN mkdir -p ~/.docker/cli-plugins && \
    mkdir -p /tmp/docker-scout/ && \
    tar xvzf /tmp/docker-scout.tar.gz -C /tmp/docker-scout/ && \
    ls -all /tmp/docker-scout/ && \
    mv /tmp/docker-scout/docker-scout ~/.docker/cli-plugins/ && \
    chmod +x ~/.docker/cli-plugins/docker-scout
ENV PATH="/${HOME}/.local/bin:/${HOME}/.docker/cli-plugins:${PATH}"

# Install NodeJS and npm from the nodejs_base stage
COPY --from=nodejs_base --chown="root:root" --chmod="0755" /usr/lib /usr/lib
COPY --from=nodejs_base --chown="root:root" --chmod="0755" /usr/local/lib /usr/local/lib
COPY --from=nodejs_base --chown="root:root" --chmod="0755" /usr/local/include /usr/local/include
COPY --from=nodejs_base --chown="root:root" --chmod="0755" /usr/local/bin /usr/local/bin

# Checks & CMD
USER ${DEFAULT_USER}:${DEFAULT_GROUP}
WORKDIR "${DEFAULT_WORKDIR}"
CMD ["/bin/bash"]

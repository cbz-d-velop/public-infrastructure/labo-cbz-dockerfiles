pipeline {
    agent any

    environment {
        DOCKER_REGISTRY = "docker-registry.nexus3.labo-cbz.net"
        DOCKER_IMAGE__DEVOPS_SONAR_SCANNER = "labocbz/devops-sonar-scanner:latest"
        DOCKER_IMAGE__DEVOPS_LINTERS = "labocbz/devops-linters:latest"
        DOCKER_IMAGE__DEVOPS_DOCKER_SCOUT = "labocbz/devops-docker-scout"
        DOCKER_IMAGE__DEVOPS_CONTAINER_STRUCTURE_TEST = "labocbz/devops-container-structure-test:latest"
        DOCKER_IMAGE__ALPINE = "alpine:latest"
    }

    options {
        buildDiscarder(
            logRotator(      
                artifactDaysToKeepStr: "",
                artifactNumToKeepStr: "",
                daysToKeepStr: "",
                numToKeepStr: "10"
            )
        )
    }

    stages {
        stage("lint") {
            parallel {
                stage("lint:dockerfile") {
                    agent {
                        docker {
                            image "${DOCKER_IMAGE__DEVOPS_LINTERS}"
                            registryUrl "https://${DOCKER_REGISTRY}"
                            registryCredentialsId "DOCKER_AUTH_CONFIG"
                            alwaysPull true
                            reuseNode true
                        }
                    }

                    steps {
                        dir("${TYPE}/${NAME}") {
                            sh('hadolint --ignore DL3018 --ignore DL3001 --ignore DL3013 --ignore DL3008 --ignore DL3009 --ignore DL3015 Dockerfile > ./hadolint.md')
                        }
                    }
                }

                stage("lint:markdown") {
                    agent {
                        docker {
                            image "${DOCKER_IMAGE__DEVOPS_LINTERS}"
                            registryUrl "https://${DOCKER_REGISTRY}"
                            registryCredentialsId "DOCKER_AUTH_CONFIG"
                            alwaysPull true
                            reuseNode true
                        }
                    }

                    steps {
                        dir("${TYPE}/${NAME}") {
                            sh('markdownlint "./README.md" --disable MD013 > ./markdownlint.md')
                        }
                    }
                }

                stage("lint:secrets") {
                    agent {
                        docker {
                            image "${DOCKER_IMAGE__DEVOPS_LINTERS}"
                            registryUrl "https://${DOCKER_REGISTRY}"
                            registryCredentialsId "DOCKER_AUTH_CONFIG"
                            alwaysPull true
                            reuseNode true
                        }
                    }

                    steps {
                        sh('detect-secrets scan --exclude-files "node_modules" --baseline .secrets.baseline')
                        sh('detect-secrets audit .secrets.baseline')
                    }
                }
            }
        }

        stage("sonarqube") {
            agent {
                docker {
                    image "${DOCKER_IMAGE__DEVOPS_SONAR_SCANNER}"
                    registryUrl "https://${DOCKER_REGISTRY}"
                    registryCredentialsId "DOCKER_AUTH_CONFIG"
                    alwaysPull true
                    reuseNode true
                }
            }

            steps {
                withCredentials([
                    string(credentialsId: "SONAR_HOST_URL", variable: "SONAR_HOST_URL"),
                    string(credentialsId: "SONAR_TOKEN", variable: "SONAR_TOKEN")
                ]) {
                    retry(3) {
                        sh('sonar-scanner')
                    }
                }
            }
        }

        stage("build") {
            agent {
                docker {
                    image "${DOCKER_IMAGE__DEVOPS_DOCKER_SCOUT}"
                    registryUrl "https://${DOCKER_REGISTRY}"
                    registryCredentialsId "DOCKER_AUTH_CONFIG"
                    alwaysPull true
                    reuseNode true
                    args '-v /var/run/docker.sock:/var/run/docker.sock'
                }
            }

            steps {
                dir("${TYPE}/${NAME}") {
                    withCredentials([
                        string(credentialsId: "DOCKER_AUTH_CONFIG_BASE64", variable: "DOCKER_AUTH_CONFIG_BASE64")
                    ]) {
                        sh('echo $DOCKER_AUTH_CONFIG_BASE64 | base64 -d > ${DOCKER_CONFIG}/config.json')
                        sh('docker login')
                        sh('bash build --test')
                    }
                }
            }
        }

        stage("test") {
            agent {
                docker {
                    image "${DOCKER_IMAGE__DEVOPS_CONTAINER_STRUCTURE_TEST}"
                    registryUrl "https://${DOCKER_REGISTRY}"
                    registryCredentialsId "DOCKER_AUTH_CONFIG"
                    alwaysPull true
                    reuseNode true
                    args '-v /var/run/docker.sock:/var/run/docker.sock'
                }
            }

            steps {
                dir("${TYPE}/${NAME}") {
                    withCredentials([
                        string(credentialsId: "DOCKER_AUTH_CONFIG_BASE64", variable: "DOCKER_AUTH_CONFIG_BASE64")
                    ]) {
                        retry(2) {
                            sh('echo $DOCKER_AUTH_CONFIG_BASE64 | base64 -d > ${DOCKER_CONFIG}/config.json')
                            sh('docker login')
                            sh('bash build --test')
                            sh('container-structure-test test --image labocbz/${NAME}:latest-test --config ./tests-config.yml')
                            sh('container-structure-test test --image labocbz/${NAME}:latest-test --config ./tests-config.yml > ./container-structure-test-report.md')
                        }
                    }
                }
            }
        }

        stage("publish") {
            agent {
                docker {
                    image "${DOCKER_IMAGE__DEVOPS_DOCKER_SCOUT}"
                    registryUrl "https://${DOCKER_REGISTRY}"
                    registryCredentialsId "DOCKER_AUTH_CONFIG"
                    alwaysPull true
                    reuseNode true
                    args '-v /var/run/docker.sock:/var/run/docker.sock'
                }
            }

            steps {
                dir("${TYPE}/${NAME}") {
                    withCredentials([
                        string(credentialsId: "DOCKER_AUTH_CONFIG_BASE64", variable: "DOCKER_AUTH_CONFIG_BASE64")
                    ]) {
                        sh('echo $DOCKER_AUTH_CONFIG_BASE64 | base64 -d > ${DOCKER_CONFIG}/config.json')
                        sh('docker login')
                        sh('bash build --cicd')
                    }
                }
            }
        }

        stage("docker-scout") {
            agent {
                docker {
                    image "${DOCKER_IMAGE__DEVOPS_DOCKER_SCOUT}"
                    registryUrl "https://${DOCKER_REGISTRY}"
                    registryCredentialsId "DOCKER_AUTH_CONFIG"
                    alwaysPull true
                    reuseNode true
                    args '-v /var/run/docker.sock:/var/run/docker.sock'
                }
            }

            steps {
                dir("${TYPE}/${NAME}") {
                    withCredentials([
                        string(credentialsId: "DOCKER_AUTH_CONFIG_BASE64", variable: "DOCKER_AUTH_CONFIG_BASE64")
                    ]) {
                        retry(2) {
                            sh('echo $DOCKER_AUTH_CONFIG_BASE64 | base64 -d > ${DOCKER_CONFIG}/config.json')
                            sh('docker-scout cves --ignore-base --exit-code --only-severity critical,high --format markdown --output ./cves-report.md labocbz/${NAME}:latest || true')
                            sh('(docker-scout recommendations labocbz/${NAME}:latest || true) > ./cves-recommendations.md')
                        }
                    }
                }
            }
        }
    }

    post {
        success {
            archiveArtifacts "${TYPE}/${NAME}/*.md"
            cleanWs(
                cleanWhenNotBuilt: true,
                deleteDirs: true,
                disableDeferredWipeout: true,
                notFailBuild: true
            )
        }

        failure {
            cleanWs(
                cleanWhenNotBuilt: true,
                deleteDirs: true,
                disableDeferredWipeout: true,
                notFailBuild: true
            )
        }
    }
}
